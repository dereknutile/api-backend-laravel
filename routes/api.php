<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;

Route::get('users', 'UserController@getAll')->name('api-users');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
