<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utilities\ProxyRequest;

class AuthController extends Controller
{
    protected $proxy;

    /**
     * Constructor
     */
    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * Register the user
     */
    public function register()
    {
        // 
    }

    /**
     * Login
     */
    public function login()
    {
        // 
    }

    /**
     * Refresh
     */
    public function refreshTo()
    {
        // 
    }

    /**
     * Sign-out
     */
    public function logout()
    {
        // 
    }
}
