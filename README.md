# API Backend Laravel

## Overview

> Laravel based backend API for any frontend consumer.

## Development

### Requirements

  * PHP
  * Git
  * Node
  * A database such as SQLite or MySQL

### Clone & Prepare

    $ cd ~/dev (or where you plan to store your code)
    $ git clone https://gitlab.com/dereknutile/laravel-api-sandbox
    $ cd laravel-api-sandbox
    $ composer install
    $ php artisan key:generate

### Generate Database Content

Create a database and set the database credentials in the `.env` file.

#### Run migrations & generate keys

    $ php artisan migrate
    $ php artisan db:seed
    $ php artisan key:generate

### Start the API Backend

    $ php artisan serve

### Website

The website is now available here: [http://localhost:8000](http://localhost:8000), however, this exercise is to write a backend that another separate application can use.

## Frontend Project(s)

Coming Soon ...
